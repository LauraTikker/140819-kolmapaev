﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kolmaspäev_140819
{
    class Program
    {
        static void Main(string[] args)
        {
            string nimi = "Laura";
            string teinenimi = "Tikker";

            nimi = nimi + " " + teinenimi;
            Console.WriteLine(nimi);

            teinenimi = "Laura Tikker";
            Console.WriteLine(nimi == teinenimi);  // JAVAs ei saa nii teha
            Console.WriteLine(nimi.Equals(teinenimi)); // JAVAs peab nii tegema
            Console.WriteLine(nimi.Equals(teinenimi, StringComparison.CurrentCultureIgnoreCase)); // Equalsiga saab võrrelda mitut moodi, equals on objektidel üleüldine võrdlemise funktsioon

            int vanus = 30;
            Console.WriteLine(nimi + " on " + vanus + " aastat vana"); // 1. võimalus. halb
            Console.WriteLine("{0} on {1} aastat vana", nimi, vanus); // 0 ja 1 on placeholderid ja asendatakse nende väärtustega 2. formaatimisega

            string teade = String.Format("{0} on {1} aastat vana", nimi, vanus); // 3. formaatimisfunktsiooniga
            Console.WriteLine(teade);

            string formatEST = "{0} on {1} aastat vana";
            string formatFIN = "{1} vuotta vanha on täma {0} poikka";

            string format = DateTime.Now.DayOfWeek == DayOfWeek.Saturday // saad vahetada placeholderit
                ? formatFIN : formatEST;

            Console.WriteLine("{0} on tore poiss kuigi {0} on {1} aastat juba vana");

            teade = String.Format("{0} on {1} aastat vana", nimi, vanus); // need kaks lauset on samad
            teade = $"{nimi} on {vanus} aastat vana"; // võimaldab placeholderid kirjutada otse stringi sisse
            Console.WriteLine(teade);

            Console.WriteLine(nimi);
            Console.WriteLine(nimi.ToUpper());
            Console.WriteLine(nimi.ToLower());
            Console.WriteLine(nimi.Split(' ')[1]); //teha tükkideks ' ' - näitab characteri ja [1] - näitab tükki
            Console.WriteLine(nimi.Substring(5, 4)); // võtta jupikesteks JAVAs teisiti c# - mitmendast märgist alates mitu (JAVAs - mitmest mitmendani 0-5)

            //string lause = "Luts kirjutab: \"Kui Arno isaga ...\" ja nii edasi"; // kasuta \ enne jutumärke, et saada see tekstina

            //string filename = "c:\\henn\\text.txt"; // kasuta kahte \\, et saada tavalise kaldkriipsuna

            // \r - kirjutab üle eelneva teksti
            // \n - uus rida
            // \t - uus tab
            // topelt "", et saada ka tavalised jutumärgid
            // @ enne teksti - üksik \ on tavaline kaldkriips


            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday) //kui jah siis teeme mis sulgude vahel
            { // Edit-Advance-Format Document trepitab õigesti vastavalt keelele (CTRL+KD
                Console.WriteLine("Jee! nädala keskel oleme");
            } // need sulud võib ka ära jätta aga semikoolonit ei tohi panna- muutub tühjaks lauseks, if-i taha ei kirjutata ;
            else if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
            {
                // täidetakse ainult reedel
                Console.WriteLine("kohe saab maale jooma minna");
            }

            else
            {
                Console.WriteLine("igav"); // neid lausedid täidetakse ainult kõigil muudel päevadel
            }

            Console.WriteLine("tavaline päev"); // siit aga iga päev

            // muutujad mis on if või else lause sees ei ole olemas/ulatus on piiratud, aga muutujaid enne saab määrata

            switch(DateTime.Now.DayOfWeek) // tingimus ja on palju varjante. juhtumtingimus
            {
                case DayOfWeek.Wednesday:
                    Console.WriteLine("lähme sauna");
                    break; // siis lõpeb siin ära, kui see on täidetud //break on kohustuslik või midagi muud
                case DayOfWeek.Saturday:
                    Console.WriteLine("lähme trenni");
                    goto case DayOfWeek.Sunday; // siis ei pane breaki ja ta läheb edasi järgmise juurde (ainult, mis sealt tuleb - kirjutab "joome õlut".
                case DayOfWeek.Monday:
                    Console.WriteLine("väike peaparandus");
                    goto default;
                case DayOfWeek.Sunday:
                    Console.WriteLine("joome õlut");
                    break;
                default: // muudeljuhtudel
                    Console.WriteLine("teeme tööd");
                    break;
            }




       



        }
    }
}
